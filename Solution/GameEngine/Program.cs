﻿using SlotMachineObj;

internal class Program
{
    private static void Main(string[] args)
    {
        var randomNumberGenerator = new RandomNumberGenerator();
        var slotMachine = new SlotMachine(randomNumberGenerator);

        Console.WriteLine("Welcome to my slot machine!" +
                          "\nAny row with only 1 symbol is a win." +
                          "\nIf you mangage to roll '*', this can supplment any other symbol!\n");

        Console.Write("Enter your deposit amount: $");
        slotMachine.SetBalance(Console.ReadLine());

        Console.Write("\nPress any key to start playing...");
        Console.ReadKey();
        Console.Clear();

        while (slotMachine.Balance != 0)
        {

            Console.WriteLine($"Current Balance: ${slotMachine.Balance}");
            Console.Write("Enter your staking amount: $");
            slotMachine.SetStakeAmount(Console.ReadLine());
            Console.Clear();

            slotMachine.Spin();
            Console.WriteLine("Spinning...");
            Thread.Sleep(400);
            Console.WriteLine("---");
            DisplayMachineScreen(slotMachine.GetReelDisplay());
            Console.WriteLine("\n---");
            Thread.Sleep(400);


            Console.Write($"You staked: ${slotMachine.StakeAmount}");
            var Msg = slotMachine.Win ? $"\nYou have won: ${slotMachine.WinningAmount}" : $"\nYou have lost: ${slotMachine.StakeAmount}";
            Console.WriteLine(Msg);
            Console.Write($"Current balance: ${slotMachine.Balance}");
            Console.Write("\nPress any key to spin again...");
            Console.ReadKey();
            Console.Clear();          
        }

        Console.Clear();
        Console.WriteLine("Game over.");
        Console.Write("Press Any key to exit.");
        Console.ReadKey();
    }

    private static void DisplayMachineScreen(char[,] display)
    {
        for (int col = 0; col < display.GetLength(0); col++)
        {
            if (col != 0)
                Console.WriteLine("");

            for (int row = 0; row < display.GetLength(1); row++)
            {
                Thread.Sleep(50);
                Console.Write(display[col, row]);
            }
        }
    }
    private static void DisplaySquare(char[,] display)
    {
        Console.WriteLine("\n  ----");
        for (int col = 0; col < display.GetLength(0); col++)
        {
            Console.WriteLine("");
            for (int row = 0; row < display.GetLength(1); row++)
            {
            }

        }
        Console.WriteLine("\n  ----");
    }

}