﻿namespace SlotMachineObj
{
    public interface ISlotMachine
    {
        decimal Balance { get; set; }
        void SetBalance(string input);
        decimal StakeAmount { get; set; }
        void SetStakeAmount(string input);
        char[,] GetReelDisplay();
        bool Win { get; set;  }
        decimal WinningAmount { get; set; }
        void Spin();
    }
}
