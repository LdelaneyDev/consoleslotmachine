﻿namespace SlotMachineObj
{
    public interface IRandomNumberGenerator
    {
        int GenerateInt(int min, int max);
        double GenerateDouble(double min, double max);
    }

    public class RandomNumberGenerator : IRandomNumberGenerator
    {
        private readonly Random _random;

        public RandomNumberGenerator()
        {
            _random = new Random();
        }

        public int GenerateInt(int min, int max)
        {
            return _random.Next(min, max);
        }

        public double GenerateDouble(double min, double max)
        {
            return _random.NextDouble() * (max - min) + min;
        }
    }
}