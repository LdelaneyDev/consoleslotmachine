﻿namespace SlotMachineObj
{
    public class SlotMachine : ISlotMachine
    {
        private readonly char[] _symbols = new char[] { 'A', 'A', 'A', 'A', 'A',
                                                        'A', 'A', 'A', 'A', 'B',
                                                        'B', 'B', 'B', 'B', 'B',
                                                        'B', 'P', 'P', 'P', '*' };

        private readonly IRandomNumberGenerator _randomNumberGenerator;

        public decimal Balance { get; set; }
        public decimal StakeAmount { get; set; }
        public char[,] ReelDisplay = new char[4, 3];
        public bool Win { get; set; }
        public decimal WinningAmount { get; set; }

        public SlotMachine(IRandomNumberGenerator randomNumberGenerator)
        {
            _randomNumberGenerator = randomNumberGenerator;
        }

        /// <summary>
        /// Returns the current reel display matrix.
        /// </summary>
        /// <returns>A 4x3 matrix containing the current reel symbols.</returns>
        public char[,] GetReelDisplay()
        {
            return ReelDisplay;
        }

        /// <summary>
        /// Updates the reel display row with the specified symbols.
        /// </summary>
        /// <param name="reel">The array of characters representing the symbols to set in the row.</param>
        /// <param name="rowNumber">The row number to set the symbols in.</param>
        private void SetReelRow(char[] reel, int rowNumber)
        {
            for (int col = 0; col < ReelDisplay.GetLength(1); col++)
                ReelDisplay[rowNumber, col] = reel[col];
        }

        /// <summary>
        /// Sets the stake amount for the player.
        /// </summary>
        /// <param name="input">The user input amount to set as the deposit amount.</param>
        public void SetStakeAmount(string input)
        {
            StakeAmount = StakingAmountVerificationLoop(input);
        }

        /// <summary>
        /// Sets the balance amount for the player.
        /// </summary>
        /// <param name="input">The user input amount to set as the deposit amount.</param>
        public void SetBalance(string input)
        {
            Balance = DepositAmountVerificationLoop(input);
        }

        /// <summary>
        /// Executes a spin of the slot machine, sets the reel display, determines if the player has won, and calculates winnings.
        /// </summary>
        public void Spin()
        {
            decimal coefficient = 0.00M;
            Win = false;

            for (int i = 0; i < 4; i++)
            {
                char[] reel = new char[3];
                for (int j = 0; j < reel.Length; j++)
                    reel[j] = _symbols[_randomNumberGenerator.GenerateInt(0, _symbols.Length)];

                SetReelRow(reel, i);

                if (IsWinningOutput(reel))
                {
                    Win = true;
                    coefficient += UpdateCoefficient(reel);
                }
            }

            if (Win)
            {
                WinningAmount = (StakeAmount * coefficient);
                Balance += WinningAmount - StakeAmount;
            }
            else
            {
                Balance -= StakeAmount;
            }
        }

        /// <summary>
        /// Updates the total coefficient for a winning spin.
        /// </summary>
        /// <param name="reel">The array of characters representing the symbols on the winning reel.</param>
        /// <returns>The updated coefficient.</returns>
        private decimal UpdateCoefficient(char[] reel)
        {
            decimal totalCoefficient = 0.00M;
            foreach (char c in reel)
            {
                switch (c)
                {
                    case 'A':
                        totalCoefficient += 0.4M;
                        break;
                    case 'B':
                        totalCoefficient += 0.6M;
                        break;
                    case 'P':
                        totalCoefficient += 0.8M;
                        break;
                    default:
                        break;
                }
            }

            return totalCoefficient;
        }

        /// <summary>
        /// Check the output of the reel to see if they all match, or have sufficient wild cards to produce a match.
        /// </summary>
        /// <param name="reel"> array of chars consisting of: A = Apple, B = Banana, P = Pineapple, * = Wildcard </param>
        private bool IsWinningOutput(char[] reel)
        {
            List<char> filteredReel = new List<char>();

            for(int r = 0; r < reel.Length; r++)
            {
                if (reel[r] != '*')
                    filteredReel.Add(reel[r]);
            }

            foreach(char r in filteredReel)
            {
                if (r != filteredReel.FirstOrDefault())
                    return false;
            }

            return true;
                
        }

        /// <summary>
        /// Continuously loops until the user input is a valid value
        /// </summary>
        /// <param name="input"> User input amount to place as a bet </param>
        private decimal StakingAmountVerificationLoop(string? input)
        {
            while (!(IsInputValid(input) && IsStakingAmountValid(input)))
            {
                input = GetUserInput();
                continue;
            }

            return decimal.Parse(input);
        }

        /// <summary>
        /// Continuously loops until the user input is a valid value
        /// </summary>
        /// <param name="input"> User input amount to set as the deposit amount </param>
        private decimal DepositAmountVerificationLoop(string? input)
        {
            while (!IsInputValid(input))
            {
                input = GetUserInput();
                continue;
            }

            return decimal.Parse(input);
        }

        /// <summary>
        /// Verifies the user input is a positive number
        /// </summary>
        /// <param name="input"> User input amount to set as the deposit amount </param>
        private bool IsInputValid(string? input)
        {
            if (!decimal.TryParse(input, out decimal output))
            {
                DisplayNumericValuePrompt();
                return false;
            }

            if (output <= 0)
            {
                DisplayPositiveValuePrompt();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verifies the user input is a positive integer and the staked amount is less than the user balance.
        /// </summary>
        /// <param name="input"> User input amount to place as a bet </param>
        private bool IsStakingAmountValid(string? input)
        {
            decimal.TryParse(input, out decimal output);

            if (output > Balance)
            {
                DisplayBalanceCheckPrompt();
                return false;
            }

            return true;
        }

        private static void DisplayNumericValuePrompt()
        {
            Console.Write("Please enter an numeric value: $");
        }

        private static void DisplayPositiveValuePrompt()
        {
            Console.Write("Please enter a positive value: $");
        }

        private static void DisplayBalanceCheckPrompt()
        {
            Console.Write("Please enter a value less than your balance: $");
        }

        private static string? GetUserInput()
        {
            return Console.ReadLine();
        }

    }
}
