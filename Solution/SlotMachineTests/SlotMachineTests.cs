using Moq;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace SlotMachineObj.Tests
{
    [TestFixture]
    public class SlotMachineTests
    {
        private Mock<IRandomNumberGenerator> _randomNumberGenerator;
        private SlotMachine _slotMachine;

        [SetUp]
        public void Setup()
        {
            _randomNumberGenerator = new Mock<IRandomNumberGenerator>();
            _slotMachine = new SlotMachine(_randomNumberGenerator.Object);
            _slotMachine.SetBalance("5000");
        }

        [Test]
        public void GetReelDisplay_ReturnsReelDisplay()
        {
            // Arrange
            char[,] expectedReel = new char[4, 3];

            // Act
            var result = _slotMachine.GetReelDisplay();

            // Assert
            Assert.AreEqual(expectedReel, result);
        }

        [Test]
        public void SetStakeAmount_ValidInput_SetsStakeAmount()
        {
            // Arrange
            var stakeInput = "2.50";
            decimal.TryParse(stakeInput,out var stakeAmount);

            // Act
            _slotMachine.SetStakeAmount(stakeInput);

            // Assert
            Assert.AreEqual(stakeAmount, _slotMachine.StakeAmount);
        }

        [Test]
        public void SetBalance_ValidInput_SetsBalance()
        {
            // Arrange
            var balanceInput = "50";
            decimal.TryParse(balanceInput, out var balanceAmount);

            // Act
            _slotMachine.SetBalance("50");

            // Assert
            Assert.AreEqual(balanceAmount, _slotMachine.Balance);
        }

        [Test]
        public void Spin_NoWin_DecreasesBalance()
        {
            // Arrange
            var expectedBalance = 3.50M;

            _randomNumberGenerator.SetupSequence(x => x.GenerateInt(0, 20))
                .Returns(0)
                .Returns(15)
                .Returns(19)
                .Returns(0)
                .Returns(15)
                .Returns(19)
                .Returns(0)
                .Returns(15)
                .Returns(19)
                .Returns(0)
                .Returns(15)
                .Returns(19);

            _slotMachine.SetStakeAmount("1.50");
            _slotMachine.SetBalance("5");

            // Act
            _slotMachine.Spin();

            // Assert
            Assert.AreEqual(expectedBalance, _slotMachine.Balance);
        }

        [Test]
        public void Spin_Win_AddsWinningAmountToBalance()
        {
            // Arrange
            var expectedBalance = 17.6M;

            _randomNumberGenerator.SetupSequence(x => x.GenerateInt(0, 20))
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0)
                .Returns(0);

            // Act
            _slotMachine.SetStakeAmount("2");
            _slotMachine.SetBalance("10");
            _slotMachine.Spin();

            // Assert
            Assert.AreEqual(expectedBalance, _slotMachine.Balance);
        }
    }
}
