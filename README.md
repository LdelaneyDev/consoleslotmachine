# <BedeSlotMachine>

## Description

This project is a slot machine simulator which takes user input and ends once the player has no more cash available.
This can be run as a fun game to introduce people to the concepts of gambling or to get experience using this specific type of machine.
I have included some basic test coverage using mocking and DI, there is potential to expand test coverage.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)
- [Tests](#tests)

## Installation

Project Dependancies:

NUnit (Installed via NuGet package manager)

Moq (Installed via NuGet package manager)

.NET6: https://dotnet.microsoft.com/en-us/download/dotnet/6.0


To install the application, either copy the SlotMachine folder at the root of this directory and play from by starting the GameEngine.exe file inside there.
Or open the BedeSlotMachine.sln file in an IDE, from there you can analyse the open source code, modify the source code and build the application for yourself.

## Usage

To play the slot machine, open the BedeSlotMachine.exe in the SlotMachine folder in the root directory.
Once opened, the user is prompted for a initial deposit balance and then a staking amount.

Once complete, the game will begin and the reels will start to spin.
The symbols displayed are A, B, P and * is a wild card which can be any symbol.
To win a spin, the row must contain all the same symbol.

The game will continue until the player has staked all their money.

## Credits

Liam Delaney

## License

WARNING This is unlincensed software, use at your own risk.
However; I, Liam Delaney, personally approve of the sharing and modification of this software.

## Tests

The project includes test coverage using NUnit and Moq to simulate dependancies and create mock objects.
To view this, open the solution in your IDE and navigate to the SlotMachineTests directory.

Within this directory you can view some of the tests I have made for the application.

I would recommend installing NCrunch from the VS2022 extensions store to run the tests in the solution.